#!/usr/bin/env python3
# run: python3 app.py

import os
import sys
import time
import toml
from dotenv import load_dotenv

mc = toml.load("conf/conf.toml")
df = toml.load(mc['data_file'])
load_dotenv()

def go(direction:str="up", amount:int=0):
    amt = range(0,amount)
    for i in amt:
        pre = f"\n" * int(max(amt) - i) if direction == 'up' else f"\n" * i
        post = f"\n" * i if direction == 'up' else f"\n" * int(max(amt) - i)
        sleep_mod = mc['sleep_modifier'] if (i == 0 and direction == 'up') or (i == max(amt) and direction == 'down') else 0
        draw(f"{pre}{df['base']}{post}{df['footer']}", float(os.getenv('MJ_TRANS_SLEEP')) + sleep_mod)

def draw(line:str="", wait:float=1):
    print(line)
    time.sleep(wait)
    # erase previous lines.
    for i in range(0,line.count('\n') + 2):
        sys.stdout.write("\033[F\033[K")

def expression(style_1:str="base", style_2:str="", style_3:str="base", wait_before:int=1, wait_action:int=.25, wait_after:int=1, top:int=0):
    pre = f"\n" * int(top - 1)
    draw(f"{pre}{df[style_1]}{df['footer']}", wait_before)
    draw(f"{pre}{df[style_2]}{df['footer']}", wait_action)
    draw(f"{pre}{df[style_3]}{df['footer']}", wait_after)

draw(df['start_message'], 3)

# @TODO: the actions should be defined in a control file.

### monkey-jump
go(direction="up", amount=mc['jump_height'])
go(direction="down", amount=mc['jump_height'])
expression(style_1="wink", style_2="blink", style_3="base", wait_before=0.33, wait_action=0.25, wait_after=1.33, top=mc['jump_height'])
expression(style_1="blink", style_2="bugeye", style_3="base", wait_before=.75, wait_action=0.25, wait_after=2.23, top=mc['jump_height'])


### kayaker (not working)
# ~ expression(style_1='base', style_2='rest', style_3='back_stroke', wait_before=1, wait_action=0.2, wait_after=0.2, top=mc['jump_height'])
# ~ expression(style_1='rest', style_2='base', style_3='rest', wait_before=0.2, wait_action=0.2, wait_after=1, top=mc['jump_height'])


### sit-to-stand
# ~ expression(style_1='base', style_2='situp_1', style_3='situp_2', wait_before=0.3, wait_action=0.3, wait_after=0.3, top=mc['jump_height'])
# ~ expression(style_1='situp_3', style_2='situp_4', style_3='situp_5', wait_before=0.3, wait_action=0.3, wait_after=0.3, top=mc['jump_height'])
# ~ expression(style_1='situp_6', style_2='situp_7', style_3='situp_8', wait_before=0.3, wait_action=0.3, wait_after=0.3, top=mc['jump_height'])
# ~ expression(style_1='situp_9', style_2='situp_10', style_3='situp_10', wait_before=0.3, wait_action=0.3, wait_after=2, top=mc['jump_height'])


### weight-lifting
# ~ expression(style_1='base', style_2='pos_1', style_3='pos_2', wait_before=0.7, wait_action=0.7, wait_after=0.7, top=mc['jump_height'])
# ~ expression(style_1='pos_3', style_2='pos_4', style_3='pos_5', wait_before=0.7, wait_action=0.7, wait_after=3, top=mc['jump_height'])
# ~ expression(style_1='pos_4', style_2='pos_3', style_3='pos_2', wait_before=0.7, wait_action=0.7, wait_after=0.7, top=mc['jump_height'])
# ~ expression(style_1='pos_1', style_2='base', style_3='pos_6', wait_before=0.7, wait_action=.7, wait_after=3, top=mc['jump_height'])

draw(line=df['end_message'], wait=3)
