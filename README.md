# README

This is a project to aid in learning basic coding which uses a Monkey (or other ascii art) to demonstrate basic coding constructs such as:

- variables
- conditional expressions (if/else)
- loops


### Setup ###

- create a virtual environment in your python root: `python3 -m virtualenv .venv`
- activate the virtualenv: `source .env-setup`
- install packages: `pip install -r requirements.txt`


### Links ###

- [ascii monkeys](https://www.asciiart.eu/animals/birds-land)
