# Developer Documentation


### Setup ###

- create a virtual environment in your project root: `python3 -m virtualenv .venv`
- activate the virtualenv: `source .env-setup`
- install packages: `pip install -r requirements.txt`


### Run Application ###

- normal run: `python3 app.py`
